# LePhare : PHotometric Analysis for Redshift Estimate
Welcome to LePhare++ (hereafter LePhare), a complete rewrite in C++ of the Fortran code [LePhare](https://www.cfht.hawaii.edu/~arnouts/LEPHARE/lephare.html).
LePHARE is a set of C++ programs to compute photometric redshifts and physical parameters by fitting spectral energy distributions (SED)
to a dataset of photometric fluxes or apparent magnitudes.

## Requirements

The C++ part of LePhare has no external dependency beyond OpenMP and standard compiling and linking libraries. The python part depends on cmake and several packages from the python scientific ecosystem (see below).

## Installation

### Build only the C++ executables with make (historical method)
Follow the steps below:

```
  git clone https://gitlab.lam.fr/Galaxies/LEPHARE
  cd LEPHARE
  export LEPHAREDIR=<path to current dir>
  export LEPHAREWORK='<path to where execution will save data>'
  cd source
  make
```
Currently, with this method, the code just builds the executables in the *source* directory.

### Build the C++ executables and the python module with cmake and setuptools
Currently, the following sequence allows to build and install the C++ executables, together with the python package.
The pybind11 header files are provided in the *extern* directory, but after a `git clone` one needs to download these as well, with the command `git submodule update --init --recursive`. We indicate below the conda environment we currently use to run the code.
```
 conda create --name lephare_env cmake=3.26.4
 conda activate lephare_env
 conda install -c conda-forge cxx-compiler
 conda install -c conda-forge python=3.10
 conda install -c conda-forge jupyterlab
 conda install -c conda-forge numpy=1.26.3
 conda install -c conda-forge matplotlib=3.8.0
 conda install -c conda-forge astropy=5.3.4
 conda install -c conda-forge scipy=1.11.4
``` 
Then, build the executables and module by executing "python setup.py install" or "python setup.py bdist_wheel". The C++ executables are installed in the bin sub directory. The python module is installed in the conventional `site-packages` area of the system. Note that you may need to create the LEPHAREWORK/filt, LEPHAREWORK/lib_bin and LEPHAREWORK/lib_mag by hand.


A user manual is provided through **LePhare_documentation.pdf** in the *doc* directory.
If Doxygen is installed (see https://www.doxygen.nl/manual/install.html), code documentation is also available: execute ```python setup.py doc``` and an html sub-directory will be available for browsing in the *doc* directory.

## Code overview and main features

LePhare consists of a set of executables :
- *filter* : read a configurable set of filters and store a representation of them for later use
- *sedtolib* : read a configurable set of SED, compute extinction corrections, and store the results into a binary library for later use
- *mag_gal* : use the preceding outputs to compute expected magnitudes, applying different rules for galaxies, stars, or QSO objects
- *zphota* : performs chisquare minimization in order to derive photometric redshifts and other physical parameters.

These executables are configurable via a set of parameters, that can be passed at the command line or through a configuration file.
The list of these parameters can be found [here](#Parameters)

## Quick Start
First, set OMP multithreaded to the value whished for, e.g. :
```
export OMP_NUM_THREADS='10'
```

Next, a parameter file named `COSMOS.para` is shipped with LePhare, together with the input files needed for a quick analysis of the COSMOS field, under the test directory:
`cd $LEPHAREDIR/test`
The typical sequence of execution is then:
- read the filters and compile them into one file stored in `$WORK/filt`
```
$LEPHAREDIR/source/filter -c COSMOS.para
```
- read the galaxy templates (as used in Ilbert et al. 2013)and store them in `$WORK/lib_bin`
```
$LEPHAREDIR/source/sedtolib -c COSMOS.para -t G -GAL_SED COSMOS_MOD.list  -GAL_LIB LIB_VISTA
```
- use the galaxy templates + filters to derive a library of predicted magnitudes and store it in `$WORK/lib_mag` (the parameters correspond to enabling emission lines correlated to UV light + free factor in scaling these lines, more information in `LePhare_documentation.pdf`)
```
$LEPHAREDIR/source/mag_gal  -c COSMOS.para -t G -GAL_LIB_IN LIB_VISTA -GAL_LIB_OUT VISTA_COSMOS_FREE -MOD_EXTINC 18,26,26,33,26,33,26,33  -EXTINC_LAW SMC_prevot.dat,SB_calzetti.dat,SB_calzetti_bump1.dat,SB_calzetti_bump2.dat  -EM_LINES EMP_UV  -EM_DISPERSION 0.5,0.75,1.,1.5,2. -Z_STEP 0.04,0,6
```
- proceed in the same way for stellar and QSO templates
```
$LEPHAREDIR/source/sedtolib -c COSMOS.para -t S -STAR_SED STAR_MOD_ALL.list
$LEPHAREDIR/source/mag_gal -c COSMOS.para -t S -LIB_ASCII YES -STAR_LIB_OUT ALLSTAR_COSMOS 

# AGN models from Salvato 2009
$LEPHAREDIR/source/sedtolib -c COSMOS.para -t Q -QSO_SED  $LEPHAREDIR/sed/QSO/SALVATO09/AGN_MOD.list
$LEPHAREDIR/source/mag_gal -c COSMOS.para -t Q -MOD_EXTINC 0,1000  -EB_V 0.,0.1,0.2,0.3 -EXTINC_LAW SB_calzetti.dat -LIB_ASCII NO  -Z_STEP 0.04,0,6
```
- finally proceed to photometric redshift estimation
```
$LEPHAREDIR/source/zphota -c COSMOS.para -CAT_IN COSMOS.in -CAT_OUT zphot_short.out -ZPHOTLIB VISTA_COSMOS_FREE,ALLSTAR_COSMOS,QSO_COSMOS  -ADD_EMLINES 0,100 -AUTO_ADAPT YES   -Z_STEP 0.04,0,6
```
- a python script is available to perform a quick diagnostics
```
python figuresLPZ.py zphot_short.out
```

- A much more extended version of this run is available in `$LEPHAREDIR/examples`. This directory contains a README_full file with extensive variation of the different keywords.

- Several examples on how to run lephare using a notebook is given in `$LEPHAREDIR/notebooks`. An run similar to the one in examples is given in example_full_run.ipynb.

## Contact
If you have comments, questions, or feedback, please contact the authors or submit an issue to the gitlab repository.

## List of keywords available for configuration of the LePhare executables {#Keywords}

All the input ascii files can have comment lines starting with #. A single configuration file can be used for all the executables.

###filter
| Keyword name | values | Comment|
| ---|:---:|---|
| FILTER_REP|  |Repository in which the filters are stored.|
| FILTER_LIST| |Comma-separated list of filter file paths relative to *FILTER_REP*.\n These files are expected to have two columns: wavelengths and corresponding transmission. |
| TRANS_TYPE| 0[def] or 1 | Define the transmission as being of type energy (0), or photon count (1). See flt::trans |
| FILTER_CALIB|0[def],1,2,3,4,5 |comma-separated list of integer corresponding in order exactly to *FILTER_LIST*, and defining which kind of calibration to apply to the filter transmission curves.  |
| FILTER_FILE| | output filename written in $LEPHAREWORK/filt |

### sedtolib
| Keyword name | values | Comment|
| ---|:---:|---|
|GAL_SED| | name of file containing the list of Galaxy SED files to include|
|GAL_FSCALE| | Arbitrary Flux Scale|
|GAL_LIB| | basename of the Galaxy SED library output bin and doc files under $LEPHAREWORK/lib_bin|
|SEL_AGE| | filename of the file containing the ages to consider|
|AGE_RANGE| min,max | age range to be considered|
|QSO_SED| | name of file containing the list of AGN/QSO SED files to include|
|QSO_FSCALE| | Arbitrary Flux Scale|
|QSO_LIB| | basename of the AGN/QSO SED library output bin and doc files under $LEPHAREWORK/lib_bin|
|STAR_SED| | name of file containing the list of Star SED files to include|
|STAR_LIB| | basename of the Star SED library output bin and doc files under $LEPHAREWORK/lib_bin|
|STAR_FSCALE| | Arbitrary Flux Scale|

### mag_gal
| Keyword name | values | Comment|
| ---|:---:|---|
|COSMOLOGY| H0,OmegaM,OmegaL| LCDM relevant cosmological keywords|
|FILTER_FILE| | same as above|
|MAGTYPE| AB[def] or VEGA | Magnitude type (AB or VEGA) of the library |
|EXTINC_LAW| calzetti.dat[def] |  Extinction laws stored in ext/. Could include several files separated by comma.|
|EB_V| 0[def] | Reddening color excess E(B-V) values to be applied. Values separated by comma.|
|MOD_EXTINC| 0,0[def] | Range of models for which extinction will be
applied. Two values per attenutation curves, separated by comma.|
|ZGRID_TYPE| 0[def] or 1| constant step in redshift if 0; evolving step in redshift as (1+z) if 1. |
|Z_STEP| 0.04,0,6 [def] | dz,zmin,zmax: redshift step (dz), the minimum (zmin) and the maximum redshift (zmax)|
|GAL_LIB_IN| | Input  galaxy library (in $ZPHOTWORK/lib_bin/)|
|QSO_LIB_IN| |  Input  AGN library (in $ZPHOTWORK/lib_bin/)|
|STAR_LIB_IN| | Input  stellar library (in $ZPHOTWORK/lib_bin/) |
|GAL_LIB_OUT| | Output galaxy library (in $ZPHOTWORK/lib_mag/)|
|QSO_LIB_OUT| | Output AGN library (in $ZPHOTWORK/lib_mag/)|
|STAR_LIB_OUT| | Output stellar library (in $ZPHOTWORK/lib_mag/)| 
|LIB_ASCII| NO[def] or YES |  Writing the magnitude library in ASCII file |
|EM_LINES| NO[def] or EMP_UV or EMP_SFR or PHYS | choice between emission line prescription   |
|EM_DISPERSION| 1[def] | Dispersion allowed in the emission line flux
factor. Exemple: 0.5,0.75,1,1.5,2 |
|ADD_DUSTEM | |Add the dust emission in templates when not included (e.g. BC03) based on energy balance.|

### zphota
| Keyword name | values | Comment|
| ---|:---:|---|
|CAT_IN| | photometric catalog input|
|INP_TYPE| F or M | Input  values:  Flux (F) or Magnitude (M)|
|CAT_TYPE| LONG[def] or SHORT |  Input catalog format (long requires at minimum spec-z and context)|
|CAT_MAG| AB[def] or VEGA | Input magnitude type|
|CAT_FMT| MEME[def] or MMEE  | Input format for photometry (MEME alternate mag-flux with error)|
|CAT_LINES| 0,10000000000[def] |  Min and max rows read in input catalog (to run only a subsample)|
|PARA_OUT| |  Name of the file with selected output parameters|
|CAT_OUT| | Name of the output file |
|ZPHOTLIB| |  Library names   (with no extension). Could have several separated by comma. Should be in LEPHAREWORK/lib_mag. |
|ADD_EMLINES| |  Range of galaxy models in which considering emission lines contribution. |
|Z_RANGE| | Limit the redshift range to be considered in the library |
|EBV_RANGE| | Limit the E(B-V) range to be considered in the library|
|ERR_SCALE| 0[def] |  Systematic errors (in mag) add in quadrature to the observations. One per filter, separated by comma.|
|ERR_FACTOR| 1.0[def]  |  Scaling factor to the errors (in flux) |
|BD_SCALE| 0[def] | Band used for scaling  the models to the observations (sum of $2^i$, as context). 0 means all.|
|GLB_CONTEXT| 0[def] | Forces the context of all objects (sum of 2^i, as context). 0 means all. |
|FORB_CONTEXT| 0[def] | context for removing some bands from the fit (sum of 2^i, as context). 0 means inactive.|
|MASS_SCALE| 0, 0[def]| Prior: allowed range in mass|
|MAG_ABS| 0, 0[def]| Prior: Absolute magnitude range allowed for the model)|
|MAG_ABS_QSO| | Absolute magnitude range acceptable for QSO library|
|MAG_REF| | Reference filter for the prior in abs. mag. (start at 1) |
|NZ_PRIOR| -1,-1[def] |  N(z) prior as function of i-band. Give the rank of i-band filter in input (start at 1). Negative value means no prior. The second number indicates which band to use if first undefined.|
|ZFIX| NO[def] or YES | Fixed redshift with the spec-z value|
|EXTERNALZ_FILE| NONE[def] | Use the spec-z from an extrenal file (format Id,zs) |
|Z_INTERP| NO[def] |  Parabolic interpolation between original step (dz)  |
|DZ_WIN| 0.25[def] | smoothing  window function for 2nd peak search in L(z) |
|MIN_THRES|  0.1[def]|  threshold for the detection of 2nd peak in normalised L(z) (between 0 and 1)|
|SPEC_OUT| NO[def]  | Output files with Gal/Star/QSO spectra (one file per object)   |
|CHI2_OUT| NO[def]  | Output files with the chi2 for the full library (one file per object)   |
|PDZ_TYPE| NONE[def] | Which type of PDF in output [BAY ZG,BAY ZQ,MIN ZG,MIN ZQ,MASS,SFR,SSFR,AGE] |
|PDZ_OUT| NONE[.pdz] | Root of the PDF output files [def-NONE] |
|FIR_LIB| NONE[def] |  Far-IR libraries separated by comma |
|FIR_LMIN| 7[def] |  $\lambda$ min for FIR analysis (in $\mu m$) |
|FIR_CONT| -1[def] |  Context for bands to be used in Far-IR   |
|FIR_SCALE| -1[def]  | Context for bands to be used for scaling |
|FIR_FREESCALE| NO[def] | Allows for free scaling |
|FIR_SUBSTELLAR| NO[def] | Removing stellar component from best optical fit |
|MABS_METHOD| 0[def] or 1 or 2 or 3 or 4 | Method used for absolute magnitudes in each filter |
|Z_METHOD| BEST[def] or ML |  compute the absolute magnitude at a given redshift solution ML or BEST |
|MABS_CONTEXT| 0[def]  | Context for the bands used to derive  Mabs. |
|MABS_REF|  1[def]  | Filter used to derive the Mabs if method=2 |
|MABS_FILT| 1[def]   | For method 4: list of  fixed filters chosen to derive Mabs in all bands according to the redshift bins |
|MABS_ZBIN| 0,6[def] | For method 4: list of Redshift bins associated with  MABS_FILT. Even number of values. |
|ADDITIONAL_MAG| |  Name of file with filters, to derive Mabs in additional filters |
|M_REF| |  Filter in which to compute the absolute magnitudes and associated errorbars |
|APPLY_SYSSHIFT| 0[def] | Apply systematic shifts in each bands. Number of values must correspond to the number of filters.  |
|AUTO_ADAPT| NO[def] |  Optimize zero-points with spec-z |
|ADAPT_BAND| 1[def] | Reference band for the selection in magnitude  |
|ADAPT_LIM| 15,35[def] | Mag range for spectro in reference band   |
|ADAPT_ZBIN| 0.01,6[def] |  Redshift's interval used for training |
|ADAPT_CONTEXT| | Context for bands used for training|
|LIMITS_ZBIN| 0.0,90.[def] | Redshift limits used to split in N bins, separated by a coma. |
|LIMITS_MAPP_REF| 1[def] | Compute z-max. Band in which the absolute magnitude is computed. |
|LIMITS_MAPP_SEL| 1[def] | Compute z-max. Give the selection band in each redshift bin.  Need 1 or N values.|
|LIMITS_MAPP_CUT| 90[def] | Compute z-max. Magnitude cut used in each redshift bin. Need 1 or N values. |
|RM_DISCREPENT_BD| 200[def] | remove band after band (max=2) until reach this threshold in chi2 |
|PROB_INTZ| | Not implemented yet in c++|
