#!/bin/bash

git config --global --add safe.directory "*"
git submodule sync
git submodule update --init --recursive
pyenv global $1
pip install --upgrade pip
pip install numpy
pip install astropy
pip install unittest
pip install pytest
python -c "import numpy as np"
python -c "import astropy"
python -c "import unittest"
pip install .
cd tests
export LEPHAREDIR=$PWD/..
export LEPHAREWORK=$PWD/WORK
python -m unittest *.py
