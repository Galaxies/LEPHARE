     3000.00         0.04 
     3005.00         0.07 
     3010.00         0.04 
     3015.00         0.03 
     3020.00         0.05 
     3025.00         0.07 
     3030.00         0.06 
     3035.00         0.05 
     3040.00         0.03 
     3045.00         0.06 
     3050.00         0.06 
     3055.00         0.05 
     3060.00         0.06 
     3065.00         0.10 
     3070.00         0.09 
     3075.00         0.10 
     3080.00         0.08 
     3085.00         0.05 
     3090.00        -0.01 
     3095.00         0.06 
     3100.00         0.06 
     3105.00         0.03 
     3110.00         0.09 
     3115.00         0.09 
     3120.00         0.09 
     3125.00         0.12 
     3130.00         0.14 
     3135.00         0.17 
     3140.00         0.28 
     3145.00         0.38 
     3150.00         0.60 
     3155.00         1.02 
     3160.00         1.54 
     3165.00         2.46 
     3170.00         3.92 
     3175.00         5.96 
     3180.00         9.04 
     3185.00        13.21 
     3190.00        18.80 
     3195.00        26.58 
     3200.00        36.78 
     3205.00        49.11 
     3210.00        65.45 
     3215.00        85.50 
     3220.00       109.94 
     3225.00       139.17 
     3230.00       173.99 
     3235.00       214.92 
     3240.00       259.58 
     3245.00       307.84 
     3250.00       363.27 
     3255.00       420.87 
     3260.00       486.93 
     3265.00       554.78 
     3270.00       630.32 
     3275.00       707.87 
     3280.00       789.81 
     3285.00       871.14 
     3290.00       961.97 
     3295.00      1046.82 
     3300.00      1136.30 
     3305.00      1227.77 
     3310.00      1321.02 
     3315.00      1407.04 
     3320.00      1495.89 
     3325.00      1584.22 
     3330.00      1670.01 
     3335.00      1749.39 
     3340.00      1833.42 
     3345.00      1911.54 
     3350.00      1986.39 
     3355.00      2055.47 
     3360.00      2132.12 
     3365.00      2192.53 
     3370.00      2256.55 
     3375.00      2313.44 
     3380.00      2368.74 
     3385.00      2418.60 
     3390.00      2467.23 
     3395.00      2509.81 
     3400.00      2553.99 
     3405.00      2592.21 
     3410.00      2625.54 
     3415.00      2656.46 
     3420.00      2684.57 
     3425.00      2705.92 
     3430.00      2731.77 
     3435.00      2755.86 
     3440.00      2772.08 
     3445.00      2789.48 
     3450.00      2799.96 
     3455.00      2809.48 
     3460.00      2819.93 
     3465.00      2826.57 
     3470.00      2829.06 
     3475.00      2825.89 
     3480.00      2825.59 
     3485.00      2818.67 
     3490.00      2818.05 
     3495.00      2809.49 
     3500.00      2795.97 
     3505.00      2779.18 
     3510.00      2764.91 
     3515.00      2745.89 
     3520.00      2720.61 
     3525.00      2695.60 
     3530.00      2671.34 
     3535.00      2640.68 
     3540.00      2612.57 
     3545.00      2578.99 
     3550.00      2541.62 
     3555.00      2508.57 
     3560.00      2465.47 
     3565.00      2425.12 
     3570.00      2381.98 
     3575.00      2335.78 
     3580.00      2285.29 
     3585.00      2239.87 
     3590.00      2185.46 
     3595.00      2133.50 
     3600.00      2078.09 
     3605.00      2017.76 
     3610.00      1956.71 
     3615.00      1895.23 
     3620.00      1832.94 
     3625.00      1772.00 
     3630.00      1706.45 
     3635.00      1638.74 
     3640.00      1570.74 
     3645.00      1503.82 
     3650.00      1431.45 
     3655.00      1363.38 
     3660.00      1290.64 
     3665.00      1220.01 
     3670.00      1146.56 
     3675.00      1076.93 
     3680.00      1006.11 
     3685.00       936.11 
     3690.00       907.57 
     3695.00       799.51 
     3700.00       733.26 
     3705.00       673.05 
     3710.00       612.90 
     3715.00       554.23 
     3720.00       497.74 
     3725.00       445.93 
     3730.00       395.63 
     3735.00       349.33 
     3740.00       302.26 
     3745.00       263.29 
     3750.00       225.01 
     3755.00       190.93 
     3760.00       159.50 
     3765.00       132.60 
     3770.00       108.72 
     3775.00        88.64 
     3780.00        69.93 
     3785.00        55.34 
     3790.00        42.90 
     3795.00        32.84 
     3800.00        24.74 
     3805.00        18.37 
     3810.00        13.25 
     3815.00         9.56 
     3820.00         6.59 
     3825.00         4.55 
     3830.00         2.95 
     3835.00         2.00 
     3840.00         1.25 
     3845.00         0.76 
     3850.00         0.48 
     3855.00         0.31 
     3860.00         0.13 
     3865.00         0.09 
     3870.00         0.05 
     3875.00         0.06 
     3880.00         0.04 
     3885.00        -0.01 
     3890.00         0.01 
     3895.00         0.05 
     3900.00         0.05 
     3905.00         0.02 
     3910.00        -0.03 
     3915.00        -0.02 
     3920.00        -0.01 
     3925.00         0.02 
     3930.00         0.05 
     3935.00         0.02 
     3940.00        -0.02 
     3945.00         0.02 
     3950.00         0.01 
     3955.00         0.02 
     3960.00         0.02 
     3965.00        -0.01 
     3970.00        -0.02 
     3975.00         0.02 
     3980.00         0.05 
     3985.00         0.02 
     3990.00         0.02 
     3995.00         0.02 
     4000.00        -0.01 
