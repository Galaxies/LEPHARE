import os

global LEPHAREDIR

try:
    LEPHAREDIR = os.environ['LEPHAREDIR']
except KeyError:
    raise RuntimeError("Environment variable LEPHAREDIR has not been set")

try:
    os.mkdir(os.environ["LEPHAREWORK"])
    os.mkdir(os.path.join(os.environ["LEPHAREWORK"], 'filt'))
    os.mkdir(os.path.join(os.environ["LEPHAREWORK"], 'lib_bin'))
    os.mkdir(os.path.join(os.environ["LEPHAREWORK"], 'lib_mag'))
    os.mkdir(os.path.join(os.environ["LEPHAREWORK"], 'zphota'))
except FileExistsError:
    pass
except KeyError:
    raise RuntimeError("Environment variable LEPHAREWORK has not been set")


from ._lephare import *
#from lephare._lephare import  get_lephare_env
#make LEPHAREDIR and LEPHAREWORK avaliable to the C++ codes
get_lephare_env()

from ._flt import *
from ._pdf import *
from ._photoz import *
from ._spec import *
from .filterSvc import *
from .magSvc import *

from .filter import *
from .sedtolib import *
from .mag_gal import *
from .zphota import *

