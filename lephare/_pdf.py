import sys, requests
import xml.dom.minidom
from ._utils import continueClass
from ._lephare import PDF
from matplotlib import pylab as plt
import numpy as np
from . import LEPHAREDIR

__all__ = ["PDF",]

@continueClass
class PDF:

    def setYvals(self, yvals, is_chi2=False):
        if is_chi2:
            self.chi2 = yvals
            self.vPDF = np.exp(-0.5*yvals)
        else:
            self.vPDF = yvals
            self.chi2 = -2*np.log(yvals)

    def plot(self, chi2=False, kwargs={}):
        if chi2:
            plt.plot(self.xaxis, self.chi2, **kwargs)
        else:
            plt.plot(self.xaxis, self.vPDF, **kwargs)

    
