import os, time
from lephare.runner import Runner
from lephare._lephare import keyword, flt, SED, get_lephare_env, write_output_filter

__all__ = ["Filter",]

global filter_config_keys
filter_config_keys = ["FILTER_REP","FILTER_LIST","TRANS_TYPE","FILTER_CALIB","FILTER_FILE"]

class Filter(Runner):
    def __init__(self, config_file=None, config_keymap=None):
        super().__init__(filter_config_keys, config_file, config_keymap)
        
    def run(self, **kwargs):
        # update keymap and verbosity based on call arguments
        # this is only when the code is called from python session
        self.verbose = kwargs.pop('verbose', self.verbose)
        for k,v in kwargs.items():
            if k.upper() in self.keymap.keys():
                self.keymap[k.upper()] = keyword(k.upper(), str(v))
            
        keymap = self.keymap
        fltRep = keymap['FILTER_REP'].split_string(os.path.join(os.environ['LEPHAREDIR'],"filt/"),1)[0]
        fltFiles = keymap["FILTER_LIST"].split_string("flt.pb",-99)
        ref_size = len(fltFiles)
        # Transmission in energy or photons
        transtyp = (keymap["TRANS_TYPE"]).split_int("0",ref_size);
        # calibration depending on the instrument
        calibtyp = (keymap["FILTER_CALIB"]).split_int("0",ref_size)
        # Output file
        outputName = keymap["FILTER_FILE"].split_string("filters",1)[0];
        filtfile = os.path.join(os.environ["LEPHAREWORK"],"filt", outputName + ".dat")
        filtdoc = os.path.join(os.environ["LEPHAREWORK"],"filt", outputName + ".doc")

        if self.verbose:
            print("#######################################")
            print("# Build the filter file with the following options: ")
            print("# Config file: {}".format(self.config))
            print("# FILTER_REP: {}".format(fltRep))
            print("# FILTER_LIST: {}".format(fltFiles))
            print("# TRANS_TYPE: {}".format(transtyp))
            print("# FILTER_CALIB: {}".format(calibtyp))
            print("# FILTER_FILE: {}".format(filtfile))
            print("# FILTER_FILE.doc: {}".format(filtdoc))
            print("#######################################")

        vecFlt = []
        for k,(f,t,c) in enumerate(zip(fltFiles,transtyp, calibtyp)):
            fltFile = os.path.join(fltRep,f)
            oneFilt = flt(k, f, t, c)
            vecFlt.append(oneFilt)
        

        write_output_filter(filtfile, filtdoc, vecFlt)


if __name__ == "__main__":
    runner = Filter()
    runner.run()
    runner.end()
