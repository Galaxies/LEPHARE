#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )";

export LEPHAREDIR=$SCRIPT_DIR/..
export LEPHAREWORK=
if [ -z "$1" ]
then
    LEPHAREWORK=$SCRIPT_DIR/WORK
else
    LEPHAREWORK=$1
fi
export LEPHAREWORK
echo $LEPHAREWORK

mkdir -p $LEPHAREWORK/filt $LEPHAREWORK/lib_bin $LEPHAREWORK/lib_mag

export OMP_NUM_THREADS='30' 

python $LEPHAREDIR/lephare/filter.py -c $LEPHAREDIR/examples/COSMOS.para

python $LEPHAREDIR/lephare/sedtolib.py -c $LEPHAREDIR/examples/COSMOS.para --typ S --STAR_SED $LEPHAREDIR/examples/STAR_MOD_ALL.list
python $LEPHAREDIR/lephare/mag_gal.py -c $LEPHAREDIR/examples/COSMOS.para --typ S --LIB_ASCII YES --STAR_LIB_OUT ALLSTAR_COSMOS 

python $LEPHAREDIR/lephare/sedtolib.py -c $LEPHAREDIR/examples/COSMOS.para --typ Q --QSO_SED  $LEPHAREDIR/sed/QSO/SALVATO09/AGN_MOD.list
python $LEPHAREDIR/lephare/mag_gal.py -c $LEPHAREDIR/examples/COSMOS.para --typ Q --MOD_EXTINC 0,1000  --EB_V 0.,0.1,0.2,0.3 --EXTINC_LAW SB_calzetti.dat --LIB_ASCII NO  --Z_STEP 0.04,0,6

python $LEPHAREDIR/lephare/sedtolib.py -c $LEPHAREDIR/examples/COSMOS.para --typ G --GAL_SED $LEPHAREDIR/examples/COSMOS_MOD.list  --GAL_LIB LIB_VISTA 
python $LEPHAREDIR/lephare/mag_gal.py  -c $LEPHAREDIR/examples/COSMOS.para --typ G --GAL_LIB_IN LIB_VISTA --GAL_LIB_OUT VISTA_COSMOS_FREE --MOD_EXTINC 18,26,26,33,26,33,26,33  --EXTINC_LAW SMC_prevot.dat,SB_calzetti.dat,SB_calzetti_bump1.dat,SB_calzetti_bump2.dat  --EM_LINES EMP_UV  --EM_DISPERSION 0.5,0.75,1.,1.5,2. --Z_STEP 0.04,0,6

python $LEPHAREDIR/lephare/zphota.py -c $LEPHAREDIR/examples/COSMOS.para --CAT_IN $LEPHAREDIR/examples/COSMOS.in --CAT_OUT zphot_short.out --ZPHOTLIB VISTA_COSMOS_FREE,ALLSTAR_COSMOS,QSO_COSMOS  --ADD_EMLINES 0,100 --AUTO_ADAPT YES   --Z_STEP 0.04,0,6 --CAT_LINES 1,100 --SPEC_OUT YES --PARA_OUT $LEPHAREDIR/examples/output.para
