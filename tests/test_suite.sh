#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )";

export LEPHAREDIR=$SCRIPT_DIR/..
export LEPHAREWORK=
if [ -z "$1" ]
then
    LEPHAREWORK=$SCRIPT_DIR/WORK
else
    LEPHAREWORK=$1
fi

export LEPHAREWORK
echo $LEPHAREWORK

mkdir -p $LEPHAREWORK/filt $LEPHAREWORK/lib_bin $LEPHAREWORK/lib_mag $LEPHAREWORK/zphota

export OMP_NUM_THREADS='30' 

$LEPHAREDIR/bin/filter -c $LEPHAREDIR/examples/COSMOS.para

$LEPHAREDIR/bin/sedtolib -c $LEPHAREDIR/examples/COSMOS.para -t S --STAR_SED $LEPHAREDIR/examples/STAR_MOD_ALL.list --LIB_ASCII YES
$LEPHAREDIR/bin/mag_gal -c $LEPHAREDIR/examples/COSMOS.para -t S --LIB_ASCII YES --STAR_LIB_OUT ALLSTAR_COSMOS 

$LEPHAREDIR/bin/sedtolib -c $LEPHAREDIR/examples/COSMOS.para -t Q --QSO_SED  $LEPHAREDIR/sed/QSO/SALVATO09/AGN_MOD.list
$LEPHAREDIR/bin/mag_gal -c $LEPHAREDIR/examples/COSMOS.para -t Q --MOD_EXTINC 0,1000  --EB_V 0.,0.1,0.2,0.3 --EXTINC_LAW SB_calzetti.dat --LIB_ASCII NO  --Z_STEP 0.04,0,6 --LIB_ASCII YES

$LEPHAREDIR/bin/sedtolib -c $LEPHAREDIR/examples/COSMOS.para -t G --GAL_SED $LEPHAREDIR/examples/COSMOS_MOD.list  --GAL_LIB LIB_VISTA

$LEPHAREDIR/bin/mag_gal  -c $LEPHAREDIR/examples/COSMOS.para -t G --GAL_LIB_IN LIB_VISTA --GAL_LIB_OUT VISTA_COSMOS_FREE --MOD_EXTINC 18,26,26,33,26,33,26,33  --EXTINC_LAW SMC_prevot.dat,SB_calzetti.dat,SB_calzetti_bump1.dat,SB_calzetti_bump2.dat  --EM_LINES EMP_UV  --EM_DISPERSION 0.5,0.75,1.,1.5,2. --Z_STEP 0.04,0,6 --LIB_ASCII YES

cat_out=zphot_short.out

$LEPHAREDIR/bin/zphota -c $LEPHAREDIR/examples/COSMOS.para --CAT_IN $LEPHAREDIR/examples/COSMOS.in --CAT_OUT $cat_out --ZPHOTLIB VISTA_COSMOS_FREE,ALLSTAR_COSMOS,QSO_COSMOS  --ADD_EMLINES 0,100 --AUTO_ADAPT YES   --Z_STEP 0.04,0,6 --CAT_LINES 1,100 --SPEC_OUT YES --PARA_OUT $LEPHAREDIR/examples/output.para --VERBOSE NO --ZFIX NO --PDZ_OUT $LEPHAREWORK/zphota/

python $LEPHAREDIR/examples/figuresLPZ.py $cat_out
python $LEPHAREDIR/examples/spec.py *.spec -d pdf -o $LEPHAREWORK/zphota/spec
mv $cat_out Id*.spec figuresLPZ.pdf $LEPHAREWORK/zphota/
